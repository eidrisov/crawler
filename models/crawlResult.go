package models

import "html/template"

type CrawlResult struct {
	Style         template.CSS
	StartUrl      string
	InternalLinks map[string]int
	ExternalLinks map[string]int
}
