# Crawler
Is a simple web crawler that analyzes only entered URL. 
## How to run
```
go mod tidy
go run main.go
```
Will download dependencies and start the server on the `8080` port.
### Assumptions
1) The crawler should analyze only the entered URL and not the entire web page.
2) The internal link is `relative url`.
3) It is assumed that inputs of type `text`/`email` and `password` within the `form` tag is a valid login form.

Valid login forms:
```
<form>
    <input type="text">
    <input type="password">
</form>

<form>
    <input type="email">
    <input type="password">
</form>
```
Invalid login form:
```
<div>
    <input type="text">
    <input type="password">
</div>
```
The application was tested using `https://id.heroku.com/login`

### Possible improvements
1) Recursively analyze all the pages.
2) Collect more metadata (prices, contact details, etc).
