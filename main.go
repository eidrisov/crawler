package main

import (
	"crawler/handlers"
	"log"
	"net/http"
)

func main() {
	err := handlers.InitTmpl()
	if err != nil {
		log.Fatalln(err)
	}

	http.HandleFunc("GET /", handlers.Index)
	http.HandleFunc("POST /crawl", handlers.Crawl)

	log.Println("starting server")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
