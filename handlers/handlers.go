package handlers

import (
	styles "crawler/css"
	"crawler/models"
	"crawler/pages"
	"crawler/spider"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

var IndexTmpl *template.Template

func InitTmpl() error {
	var err error
	IndexTmpl, err = template.New("index").Parse(pages.Index)
	return err
}

func Index(w http.ResponseWriter, r *http.Request) {
	indexTmplObj := models.CrawlResult{
		Style: template.CSS(styles.Main),
	}

	err := IndexTmpl.Execute(w, indexTmplObj)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("failed to serve index file"))
	}
}

func Crawl(w http.ResponseWriter, r *http.Request) {
	startUrl := r.PostFormValue("baseUrl")
	spider := spider.NewSpider(startUrl)
	res, _, err := spider.Crawl()
	if err != nil {
		log.Println(err)
	}

	res.Style = template.CSS(styles.Main)
	res.StartUrl = startUrl

	err = IndexTmpl.Execute(w, res)
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("failed to serve index file"))
	}
}
