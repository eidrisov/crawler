package spider

import (
	"net/url"
	"regexp"
	"strings"
)

func isRelativeLink(link string) bool {
	if link[0] == '/' {
		return true
	}
	return false
}

func isExternalLink(link string, baseUrl string) bool {
	if link[0] == '/' {
		return false
	}
	if strings.Contains(link, baseUrl) {
		return false
	}
	return true
}

func normalize(newUrl string) (string, error) {
	u, err := url.Parse(newUrl)
	if err != nil {
		return "", err
	}

	normUrl := u.Host + u.Path
	if normUrl[len(normUrl)-1] == '/' {
		normUrl = normUrl[:len(normUrl)-1]
	}
	return normUrl, nil
}

func getHtmlVersion(bs []byte) string {
	var validDoctype = regexp.MustCompile(`(?i)<!doctype\s.*>`)
	var validHtml5 = regexp.MustCompile(`(?i)<!doctype\s+html>`)
	var validOldHtml = regexp.MustCompile(`(?i)<!doctype\s+html.*dtd (html|xhtml) [0-5]{1}[.][0-9]{1,2}.*>`)
	var validOldHtmlVersion = regexp.MustCompile(`[0-5]{1}[.][0-9]{1,2}`)

	docType := validDoctype.FindString(string(bs))
	if docType != "" {
		if validHtml5.Match([]byte(docType)) {
			return "5"
		} else if validOldHtml.Match([]byte(docType)) {
			return validOldHtmlVersion.FindString(string(bs))
		}
	}
	return ""
}
