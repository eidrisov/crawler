package spider

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/net/html"
)

func TestAnalyze1(t *testing.T) {
	htmlStr := `<!DOCTYPE html>
	<html>
		<head>
			<title>Page Title</title>
		</head>
	<body>
		<p>My first paragraph.</p>
	</body>
	</html>`

	s := NewSpider("https://books.com")
	doc, err := html.Parse(strings.NewReader(htmlStr))
	assert.Equal(t, nil, err, "unexpected error while parsing html")

	s.analyze(doc)
	assert.Equal(t, s.Title, "Page Title", "wrong title")
}

func TestAnalyze2(t *testing.T) {
	htmlStr := `<!DOCTYPE html>
	<html>
		<head>
			<title>Page Title</title>
		</head>
	<body>
		<h1>First h1 header</h1>
		<h1>Second h1 header</h1>
		<h3>H3 header</h3>
	</body>
	</html>`

	s := NewSpider("https://books.com")
	doc, err := html.Parse(strings.NewReader(htmlStr))
	assert.Equal(t, nil, err, "unexpected error while parsing html")

	s.analyze(doc)
	assert.Equal(t, s.Headers["h1"], 2, "expected 2 h1 headers")
	assert.Equal(t, s.Headers["h2"], 0, "expected 0 h2 headers")
	assert.Equal(t, s.Headers["h3"], 1, "expected 1 h3 header")
	assert.Equal(t, s.Headers["h4"], 0, "expected 0 h4 headers")
	assert.Equal(t, s.Headers["h5"], 0, "expected 0 h5 headers")
	assert.Equal(t, s.Headers["h6"], 0, "expected 0 h6 headers")
}

func TestAnalyze3(t *testing.T) {
	htmlStr := `<!DOCTYPE html>
	<html>
		<head>
			<title>Page Title</title>
		</head>
	<body>
		<a href="/Layla-and-Majnun">Layla and Majnun</a>
		<a href="/Eugene-Onegin">Eugene Onegin</a>
		<a href="https://twitter.com">Follow us on twitter</a>
	</body>
	</html>`

	s := NewSpider("https://books.com")
	doc, err := html.Parse(strings.NewReader(htmlStr))
	assert.Equal(t, nil, err, "unexpected error while parsing html")

	s.analyze(doc)
	assert.Equal(t, len(s.Internal), 2, "expected 2 internal links")
	assert.Equal(t, len(s.External), 1, "expected 1 external links")
}

func TestAnalyze4(t *testing.T) {
	htmlStr := `<!DOCTYPE html>
	<html>
		<head>
			<title>Page Title</title>
		</head>
	<body>
		<form>
			<input type="text" name="username">
			<input type="password" name="password">
		</form>
	</body>
	</html>`

	s := NewSpider("https://books.com")
	doc, err := html.Parse(strings.NewReader(htmlStr))
	assert.Equal(t, nil, err, "unexpected error while parsing html")

	s.analyze(doc)
	assert.Equal(t, true, s.ContainsLoginForm, "expected html to contain login form")
}

func TestAnalyze5(t *testing.T) {
	htmlStr := `<!DOCTYPE html>
	<html>
		<head>
			<title>Page Title</title>
		</head>
	<body>
		<form>
			<div>
				<input type="text" name="username">
				<input type="password" name="password">
			</div>
		</form>
	</body>
	</html>`

	s := NewSpider("https://books.com")
	doc, err := html.Parse(strings.NewReader(htmlStr))
	assert.Equal(t, nil, err, "unexpected error while parsing html")

	s.analyze(doc)
	assert.Equal(t, true, s.ContainsLoginForm, "expected html to contain login form")
}

func TestAnalyze6(t *testing.T) {
	htmlStr := `<!DOCTYPE html>
	<html>
		<head>
			<title>Page Title</title>
		</head>
	<body>
		<form>
			<div>
				<input type="text" name="username">
			</div>
			<div>
				<input type="password" name="password">
			</div>
		</form>
	</body>
	</html>`

	s := NewSpider("https://books.com")
	doc, err := html.Parse(strings.NewReader(htmlStr))
	assert.Equal(t, nil, err, "unexpected error while parsing html")

	s.analyze(doc)
	assert.Equal(t, true, s.ContainsLoginForm, "expected html to contain login form")
}

func TestAnalyze7(t *testing.T) {
	htmlStr := `<!DOCTYPE html>
	<html>
		<head>
			<title>Page Title</title>
		</head>
	<body>
		<form>
			<input type="text" name="username">
		</form>
	</body>
	</html>`

	s := NewSpider("https://books.com")
	doc, err := html.Parse(strings.NewReader(htmlStr))
	assert.Equal(t, nil, err, "unexpected error while parsing html")

	s.analyze(doc)
	assert.Equal(t, false, s.ContainsLoginForm, "expected html not to contain login form")
}

func TestAnalyze8(t *testing.T) {
	htmlStr := `<!DOCTYPE html>
	<html>
		<head>
			<title>Page Title</title>
		</head>
	<body>
		<form>
			<input type="password">
		</form>
	</body>
	</html>`

	s := NewSpider("https://books.com")
	doc, err := html.Parse(strings.NewReader(htmlStr))
	assert.Equal(t, nil, err, "unexpected error while parsing html")

	s.analyze(doc)
	assert.Equal(t, false, s.ContainsLoginForm, "expected html not to contain login form")
}
