package spider

import (
	"bytes"
	"crawler/models"
	"errors"
	"io"
	"log"
	"net/http"
	"time"
	"sync"

	"golang.org/x/net/html"
)

func NewSpider(baseUrl string) *Spider {
	return &Spider{
		baseUrl:       baseUrl,
		client:        &http.Client{},
		InternalLinks: make(map[string]int, 0),
		ExternalLinks: make(map[string]int, 0),
	}
}

type Spider struct {
	host          string
	baseUrl       string
	client        *http.Client
	InternalLinks map[string]int
	ExternalLinks map[string]int
	sync.Mutex
}

func (s *Spider) Crawl() (models.CrawlResult, int, error) {
	t := time.Now()
	body, code, err := s.doReq(s.baseUrl, true)
	if err != nil {
		return models.CrawlResult{}, code, err
	}

	doc, err := html.Parse(bytes.NewBuffer(body))
	if err != nil {
		return models.CrawlResult{}, 500, errors.New("failed to parse html content")
	}
	s.analyze(doc)

	res := models.CrawlResult{
		StartUrl:      s.baseUrl,
		InternalLinks: s.InternalLinks,
		ExternalLinks: s.ExternalLinks,
	}
	log.Println(time.Since(t))

	return res, 200, nil
}

func (s *Spider) analyze(n *html.Node) {
	if n.Type == html.ElementNode && n.Data == "a" {
		for _, a := range n.Attr {
			if a.Key == "href" {
				link := a.Val
				if isRelativeLink(link) {
					link = s.baseUrl + link

					if _, ok := s.InternalLinks[link]; ok {
						s.InternalLinks[link]++
						continue
					}
					s.setLink(link, true)
					log.Println("actively crawling", link)

					body, _, err := s.doReq(link, true)
					if err != nil {
						continue
					}

					doc, err := html.Parse(bytes.NewBuffer(body))
					if err != nil {
						continue
					}
					go s.analyze(doc)
				} else if isExternalLink(link, s.baseUrl) {
					s.setLink(link, false)
				}
			}
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		s.analyze(c)
	}
}

func (s *Spider) doReq(pageUrl string, readBody bool) ([]byte, int, error) {
	resp, err := http.Get(pageUrl)
	if err != nil {
		return nil, 400, err
	}

	if resp.StatusCode > 399 {
		return nil, resp.StatusCode, errors.New("inaccessible link")
	}

	if readBody {
		defer resp.Body.Close()
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, 500, errors.New("could not read page content")
		}
		return body, 200, nil
	}

	return []byte{}, 200, nil
}

func (s *Spider) setLink(link string, isInternal bool) {
	s.Lock()
	defer s.Unlock()

	if isInternal {
		if _, ok := s.InternalLinks[link]; ok {
			s.InternalLinks[link]++
		} else {
			s.InternalLinks[link] = 1
		}
	} else {
		if _, ok := s.ExternalLinks[link]; ok {
			s.ExternalLinks[link]++
		} else {
			s.ExternalLinks[link] = 1
		}
	}
}
