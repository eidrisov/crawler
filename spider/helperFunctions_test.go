package spider

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsRelativeLink1(t *testing.T) {
	res := isRelativeLink("/white-fang")
	assert.Equal(t, true, res, "should be relative link")
}

func TestIsRelativeLink2(t *testing.T) {
	res := isRelativeLink("http://books.com/white-fang")
	assert.Equal(t, false, res, "should be relative link")
}

func TestNormalize1(t *testing.T) {
	startUrl := "http://books.com"
	expected := "books.com"
	normUrl, err := normalize(startUrl)
	assert.Equal(t, nil, err, "unexpected url parsing error")
	assert.Equal(t, expected, normUrl, "incorrect normalization result")
}

func TestNormalize2(t *testing.T) {
	startUrl := "http://books.com/code-davinci"
	expected := "books.com/code-davinci"
	normUrl, err := normalize(startUrl)
	assert.Equal(t, nil, err, "unexpected url parsing error")
	assert.Equal(t, expected, normUrl, "incorrect normalization result")
}

func TestNormalize3(t *testing.T) {
	startUrl := "http://books.com/code-davinci/"
	expected := "books.com/code-davinci"
	normUrl, err := normalize(startUrl)
	assert.Equal(t, nil, err, "unexpected url parsing error")
	assert.Equal(t, expected, normUrl, "incorrect normalization result")
}

func TestGetHtmlVersion1(t *testing.T) {
	version := `<!DOCTYPE html>`
	res := getHtmlVersion([]byte(version))
	assert.Equal(t, "5", res, "incorrect html version")
}

func TestGetHtmlVersion2(t *testing.T) {
	version := `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "https://www.w3.org/TR/html4/strict.dtd">`
	res := getHtmlVersion([]byte(version))
	assert.Equal(t, "4.01", res, "incorrect html version")
}

func TestGetHtmlVersion3(t *testing.T) {
	version := `<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN">`
	res := getHtmlVersion([]byte(version))
	assert.Equal(t, "4.0", res, "incorrect html version")
}

func TestGetHtmlVersion4(t *testing.T) {
	version := `<!DOCTYPE notAValidVersion>`
	res := getHtmlVersion([]byte(version))
	assert.Equal(t, "", res, "incorrect html version")
}

// func TestNormalize4(t *testing.T) {
// 	startUrl := "http://books.com/code-davinci/"
// 	expected := "books.com/sword-of-destiny"
// 	s := NewSpider(startUrl)
// 	s.setHost()
// 	normUrl, err := s.normalize("/sword-of-destiny")
// 	assert.Equal(t, nil, err, "unexpected url parsing error")
// 	assert.Equal(t, expected, normUrl, "incorrect normalization result")
// }
